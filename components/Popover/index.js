import Popover from './main.vue'
import directive from './directive'
import Vue from 'vue'
import './style/style.styl'
Vue.directive('popover', directive)

/* istanbul ignore next */
Popover.install = function(Vue) {
  Vue.directive('popover', directive)
  Vue.component(Popover.name, Popover)
}
Popover.directive = directive

export default Popover
