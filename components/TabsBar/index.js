import TabsBar from './main.vue'
/* istanbul ignore next */
TabsBar.install = function(Vue) {
  Vue.component(TabsBar.name, TabsBar)
}

export default TabsBar
