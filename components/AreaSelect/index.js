import AreaSelect from './main.vue'
/* istanbul ignore next */
AreaSelect.install = function(Vue) {
  Vue.component(AreaSelect.name, AreaSelect)
}

export default AreaSelect
