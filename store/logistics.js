/**
 * store logistics
 */
import request from '~/service/request'
export const state = () => ({
  orders: {
    list: [],
    count: 0
  }
})
export const getters = {
  orders: state => state.orders || {}
}
export const actions = {}
export const mutations = {
  SET_STATE(state, payload) {
    const { target, data } = payload
    state[target] = data
  }
}
