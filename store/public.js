/**
 * store logistics
 */
import request from '~/service/request'
export const state = () => ({
  orderDetail: {
    platformOrder: {},
    platformOrderLogList: []
  },
  warehouseReceiverAddressList: []
})
export const getters = {
  orderDetail: state => state.orderDetail,
  warehouseReceiverAddressList: state => state.warehouseReceiverAddressList
}
export const actions = {
  // 订单详情
  async orderDetail({ commit }, id) {
    try {
      const { data } = await request.get(`/api/platformOrder/${id}`)
      commit('SET_STATE', { data, target: 'orderDetail' })
      return data
    } catch (error) {
      throw error
    }
  },
  // 仓库地址列表
  async getWarehouseReceiverAddressList({ commit }) {
    try {
      const { data } = await request.get(`/api/warehouse/receiverAddress/list`)
      commit('SET_STATE', { data, target: 'warehouseReceiverAddressList' })
      return data
    } catch (error) {
      throw error
    }
  },
  // 校验手机号
  async registerCheckPhone({}, phone) {
    try {
      return await request.get(`/api/customer/phone/${phone}`)
    } catch (error) {
      throw error
    }
  },
  // 登录
  async login({}, params = {}) {
    try {
      const { data } = await request.post('/api/login', params)
      localStorage.setItem('user', JSON.stringify(data))
      return data || {}
    } catch (error) {
      throw error
    }
  },
  // 登出
  async logout() {
    try {
      await request.get('/api/logout')
      localStorage.removeItem('user')
    } catch (error) {
      throw error
    }
  },
  // 忘记密码
  async customerForgetPassword({}, params = {}) {
    try {
      return await request.put('/api/customer/forgetPassword', params)
    } catch (error) {
      throw error
    }
  },
  async uploadParams({}) {
    try {
      const { data } = await request.get('/api/upload/uploadParams')
      return data
    } catch (error) {
      console.log(error)
      throw error
    }
  }
}
export const mutations = {
  SET_STATE(state, payload) {
    const { target, data } = payload
    state[target] = data
  }
}
