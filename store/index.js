export const state = () => ({
  sidebar: false,
  layoutLoginDisable: false,
  province: {}
})
export const getters = {
  layoutLoginDisable: state => state.layoutLoginDisable
}
export const mutations = {
  TOGGLE_SLIDEBAR(state) {
    state.sidebar = !state.sidebar
  },
  LAYOUT_LOGIN_DISABLE(state) {
    state.layoutLoginDisable = !state.layoutLoginDisable
  },
  SET_PROVINCE(state, { pinyin, data }) {
    state.province[pinyin] = data
  }
}
