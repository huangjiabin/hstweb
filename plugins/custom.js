import Vue from 'vue'
import 'babel-polyfill'
import ViewBg from '~/components/ViewBg'
import * as filters from '~/utils/filters'
import { resetFields } from '~/utils/utils'
import Loading from '~/components/Loading'
import Message from '~/components/Message'
import * as request from '~/service'
for (let key in filters) {
  Vue.filter(key, filters[key])
}
Vue.prototype.$resetFields = resetFields
Vue.prototype.$loading = Loading.service
Vue.prototype.$message = Message
Vue.prototype.$http = request
Vue.use(Loading.directive)
Vue.component('ViewBg', ViewBg)
