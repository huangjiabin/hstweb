import Vue from 'vue'
import Vuetify, {
  VApp,
  VCard,
  VList,
  VBtn,
  VIcon,
  VToolbar,
  VDialog,
  VForm,
  VTextField,
  VDivider,
  VCarousel,
  VRadioGroup,
  VMenu,
  VStepper
} from 'vuetify/lib'
import colors from 'vuetify/es5/util/colors'
import { Resize, Scroll } from 'vuetify/lib/directives'

Vue.use(Vuetify, {
  components: {
    VApp,
    VCard,
    VList,
    VBtn,
    VIcon,
    VToolbar,
    VDialog,
    VForm,
    VTextField,
    VDivider,
    VCarousel,
    VRadioGroup,
    VMenu,
    VStepper
  },
  directives: {
    Resize,
    Scroll
  },
  theme: {
    primary: '#91c42a', // a color that is not in the material colors palette
    accent: colors.grey.darken3,
    secondary: colors.amber.darken3,
    info: '#1e98ff',
    warning: '#fcbf19',
    error: '#ff4141',
    success: '#4eb940',
    barcolor: '#000',
    'light-barcolor': '#313744',
    'font-default-color': '#263240',
    'medium-grey-color': '#8492a6',
    'light-grey-color': '#bdc5d0'
  }
})
