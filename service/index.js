import axios from './request'
// 注册
export const register = params => axios.post('/api/customer/register', params)
// 登录
export const login = params => axios.post('/api/login', params)
// 手机校验
export const registerCheckPhone = phone =>
  axios.get(`/api/customer/phone/${phone}`)
// 忘记密码
export const customerForgetPassword = params =>
  axios.put('/api/customer/forgetPassword', params)
// 获取上传参数
export const uploadParams = () => axios.get('/api/upload/uploadParams')
// 全国服务网点省份
export const outletProvinceList = () =>
  axios.get('/api/index/outlet/provinceList')
// 全国服务网点市区查询
export const outletDetailByProvinceById = id =>
  axios.get(`/api/index/outlet/serviceOutletDetailByProvince/${id}`)
// 首页快速查件
export const searchOrderLogOrderId = id =>
  axios.get(`/api/index/searchOrderLog/${id}`)
// 仓库收货地址列表
export const warehouseReceiverAddressList = () =>
  axios.get(`/api/warehouse/receiverAddress/list`)
// 物流商网点列表
export const outletLogisticsList = params =>
  axios.post('/api/outlet/logistics/list', params)
// 全部网点
export const outletAll = () => axios.get('/api/outlet/all')
// 物流商订单列表
export const platformOrderLogisticsList = params =>
  axios.post('/api/platformOrder/logistics/list', params)
// 物流商网点地址列表
export const outletLogisticsAddressList = params =>
  axios.get('/api/outlet/logistics/addressList', params)
// 物流商收件
export const platformOrderLogistics = params =>
  axios.post('/api/platformOrder/logistics', params)
// 个人用户订单列表
export const platformOrderPersonList = params =>
  axios.post('/api/platformOrder/person/list', params)
// 企业用户订单列表
export const platformOrderCompanyList = params =>
  axios.post('/api/platformOrder/company/list', params)

export const platformOrderCustomer = params =>
  axios.post('/api/platformOrder/customer', params)
