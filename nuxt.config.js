const nodeExternals = require('webpack-node-externals')
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')
const proxyConfig = require('./proxy.config')
module.exports = {
  mode: 'universal',
  server: {
    port: 5050, // default: 3000
    host: '0.0.0.0' // default: localhost,
  },
  /*
  ** Headers of the page
  */
  head: {
    title: '海速通 | 海速跨境平台',
    titleTemplate: '%s - 海速通 | 海速跨境平台',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'keywords', content: '海速通,海速跨境平台,hst' },
      {
        hid: 'description',
        name: 'description',
        content: '海速通,海速跨境平台'
      },
      { name: 'author', content: 'csjiabin <qq1187712426@gmail.com>' },
      {
        name: 'google-site-verification',
        content: 'Xk9YWz2-rBE9P5__qdWz3nUr4zb9bIDSdANHpezjB04'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: '/iconfont/iconfont.css' },
      { rel: 'canonical', href: 'https://www.example.com/' }
    ],
    script: [
      { src: '/iconfont/iconfont.js' },
      {
        // src: 'https://hm.baidu.com/hm.js?2620e69674d25088f870bef114e8ff89'
      } /* 引入百度统计的js */
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: {
    color: '#91c42a'
  },

  /*
  ** Global CSS
  */
  css: ['~/assets/style/app.styl'],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    // '~plugins/mockjs',
    { src: '~/plugins/ga.js', ssr: false },
    '~/plugins/vuetify.js',
    '~/plugins/custom.js',
    '~/plugins/amap.js'
  ],
  router: {
    // middleware: 'auth'
  },
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    proxy: true,
    // prefix: '/api',
    retry: { retries: 3 }
  },
  proxy: proxyConfig,
  workbox: {
    runtimeCaching: [
      {
        urlPattern: 'http://hstweb.hjbnice.com/.*',
        handler: 'cacheFirst'
      }
    ]
  },
  /*
  ** Build configuration
  */
  build: {
    transpile: [/^vuetify/],
    plugins: [new VuetifyLoaderPlugin()],
    extractCSS: true,
    postcss: {
      'postcss-import': {},
      'postcss-url': {},
      // to edit target browsers: use "browserslist" field in package.json
      preset: {
        autoprefixer: {
          browsers: [
            '> 1%',
            'last 2 versions',
            'not ie <= 20',
            'not ie_mob <= 100',
            'not ff <= 100',
            'not and_ff <= 100',
            'not Edge <= 100',
            'Android >= 4.0'
          ]
        }
      }
    },
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      if (process.server) {
        config.externals = [
          nodeExternals({
            whitelist: [/^vuetify/]
          })
        ]
      }
    }
  }
}
