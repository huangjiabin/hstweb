/**
 * @author csjiabin <qq1187712426@gmail.com>
 * @param {Object} fields
 * @param {Array} ignores
 * @return {Object}
 */
export const resetFields = (fields = {}, ignores = []) => {
  if (!fields) return {}
  const type = Object.prototype.toString
  let newFields = { ...fields }
  Object.entries(newFields).forEach(([key, value]) => {
    if (!ignores.includes(key)) {
      if (type.call(value) === '[object String]') newFields[key] = ''
      if (type.call(value) === '[object Number]') newFields[key] = null
      if (type.call(value) === '[object Array]') newFields[key] = []
      if (type.call(value) === '[object Boolean]') newFields[key] = false
      if (type.call(value) === '[object Null]') newFields[key] = null
      if (type.call(value) === '[object Undefined]') newFields[key] = null
      if (type.call(value) === '[object Object]') newFields[key] = {}
      if (type.call(value) === '[object Function]') newFields[key] = null
    }
  })
  return newFields
}

export const asyncScript = (u, c) => {
  if (!process.client) return
  let d = document
  let s = d.getElementsByTagName('script')[0]
  if (!!s.getAttribute('script-src')) return
  let o = d.createElement('script')
  o.src = u
  o.setAttribute('script-src', u)
  if (c) {
    o.addEventListener(
      'load',
      e => {
        c(null, e)
      },
      false
    )
  }
  s.parentNode.insertBefore(o, s)
}

export const Uuid = () => {
  return Math.random()
    .toString(36)
    .slice(4)
}
