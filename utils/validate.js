export const floatValid = (v, label, args = {}) => {
  let { noNull = false, num = 2, desc = '两' } = args
  let reg = new RegExp(`^[0-9]+([.]{1}[0-9]{1,${num}})?$`, 'gi')
  if (!v && noNull) return `${label}不能为空`
  if (!v && !noNull) return true
  return (reg.test(v) && v > 0) || `${label}为正整数或${desc}位小数`
}
