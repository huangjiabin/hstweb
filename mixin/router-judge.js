import initMixin from './init-mixin.js'

export default {
  mixins: [initMixin],
  methods: {
    judgeSkip() {
      const { path: uri } = this.$route
      if (uri === '/' || uri === '/index') this.$router.push('/expres-single')
      if (!this.getUser()) this.isMyPage && this.$router.push('/expres-single')
    }
  },
  computed: {
    isMyPage() {
      return this.$route.path.includes('/my')
    },
    isRegisterPage() {
      return this.$route.path.includes('/register')
    },
    isForgetPage() {
      return this.$route.path.includes('/forget')
    }
  },
  watch: {
    $route() {
      if (this.isRegisterPage || this.isForgetPage) {
        if (process.client) localStorage.removeItem('user')
      }
      this.judgeSkip()
    }
  },
  mounted() {
    this.judgeSkip()
  }
}
